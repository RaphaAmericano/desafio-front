### DESAFIO ###

#### Instruções gerais ####

Faça um fork desse projeto e desenvolva seu código nele.
Utilize o README para nos passar quaisquer informações que julgar
interessante/necessárias (como rodar o projeto, decisões tomadas, etc).

Não definimos um tempo para o desafio ser feito, ***é você quem deve nos
passar uma data limite para a entrega do desafio***.

#### O desafio ####

O desafio consiste em criar uma aplicação web que permita a navegação
entre telas que forneçam informações de super heróis da Marvel.
Esperamos navegar entre 2 telas. São elas:

- Home com todos os personagens e filtro
- Detalhe do personagem

A proposta aqui é que você tenha liberdade total de criar a home e que
siga o layout proposto (encontram-se no diretório `layout`) para o
filtro e detalhe do personagem.

O filtro deve ser uma caixa localizada na home e o resultado do filtro
pode ser listado nesta mesma tela.
O resultado do filtro deve ser sempre uma listagem de ***personagens***
de acordo com as opções clicadas.
No 1º nível deve ser possível escolher entre `comics`, `events` ou `series`.
O 2º nível deve mostrar as opções de acordo com a escolha do 1º nível.
Ambos o níveis devem permitir a seleção de mais de uma opção.

Seria interessante tratar casos como:

- O que acontece caso uma das opções de 1º nível não tenha resultados
para mostrar no 2º nível?
- Como fico sabendo no resultado qual filtro retornou determinado personagem?

As informações dos personagens podem ser encontradas [na API da Marvel](
https://developer.marvel.com/docs#!/public/getCreatorCollection_get_0).
Será necessário criar uma conta junto a eles para gerar as api_keys que
lhe darão acesso (leia mais [aqui](https://developer.marvel.com/documentation/authorization))
aos endpoints a serem utilizados no projeto.

Essa é a base do desafio, você pode nos surpreender com quaiquer detalhes que quiser. =)


***O que nós esperamos do seu desafio:***

- Usar React
- Usar ES6
- Testes unitários
- Aplicação dividida em componentes claros, de responsabilidade única e
facilmente reutilizáveis
- Tratamento adequado de possíveis erros

***O que nos impressionaria (famoso bônus):***

- Redux
- Alguma metodologia para definição e organização do seu código HTML/
CSS e identificá-la (ex: [BEM naming convention](http://getbem.com/naming/))
- Disponibilizar o projeto em uma plataforma como Heroku, AWS, GAE, etc
- Testes funcionais automatizados
- ES7 (async/await)

***O que avaliaremos de seu desafio:***

- Histórico de commits do git
- As instruções de como rodar o projeto
- Organização, semântica, estrutura, legibilidade, manutenibilidade do
seu código
- Alcance dos objetivos propostos
- Componentização e extensibilidade dos componentes Javascript

#### IPC ####

Levamos MUITO a sério código limpo e organizado. Nossa dica é: Olhe um
código que tenha escrito a menos de um mês e reflita. Sinto-me orgulhoso
por ter escrito essas linhas de código? Se a resposta for não, considere
a possibilidade de se atualizar antes de entregar o desafio. Os livros a
seguir podem ajudar nessa jornada:

- Código Limpo. Habilidades Práticas Do Agile Software
- Refactoring: Improving the Design of Existing Code
- TDD. Desenvolvimento Guiado por Testes
- O Programador Pragmático: De Aprendiz a Mestre
- O Codificador Limpo
- Practices of an Agile Developer: Working in the Real World

Bad smells muito comuns que vemos em nossos desafios:

-  Código duplicado.
-  Código sem testes automatizados.
-  Funções/métodos longos (mais de 10 linhas).
-  Classes muito longas (mais de 20 métodos).
-  Muitos if/else.
-  Uso de estruturas de dados inadequadas. for barcode in {12345}: por exemplo.
-  Funções/métodos com muitos parâmetros (mais de 3).
-  Console.log
-  Retornar estado de erro em vez de levantar exceção.
-  Não prover instruções de instalação (readme, makefile...). Quem faz com Docker ganha uma barra de chocolate!


### SOBRE A VAGA ###

***O que um “Siever” faz?***

- Desenvolvimento e manutenção de aplicações e componentes em modelo
SPA, utilizando React JS
- Condução de triagem de bugs e exceções em produção;
- Automação e simplificação de processos já existentes;
- Revisão de código e atividades de garantia de qualidade.

***Requisitos***

- Tem uma base sólida de “Vanilla” Javascript, HTML e CSS;
- Se mantém sempre atualizado sobre tecnologias frontend;
- Consegue trabalhar com independência e autonomia de decisões, sendo
capaz de discutir a solução de problemas com os membros do time e
assumir responsabilidade sobre o sucesso do negócio;
- Tem boa escrita e leitura em inglês, sendo capaz de escrever código e
 documentação;
- É capaz de pensar em soluções genéricas e modulares, estruturando seu
 desenvolvimento em componentes e gerando uma base de código
 reutilizável, bem documentada e de fácil compreensão;
- Acredita que testes (unitários, funcionais, integração,…) são
necessários e não desenvolve código que não possa ser testado;
- Sabe lidar com uma codebase (Backbone + React) existente, reescrevendo
 e renovando aplicações e componentes e gerando melhorias na
 manutenabilidade e facilidade de implementação de novas features;
- Está familiarizado com todo o ciclo de vida de desenvolvimento de
software e o ferramental necessário para transpilação de código,
gerenciadores de pacotes. Automatização de build, de testes e tarefas;
- Possui boas noções de UX, sendo capaz de colaborar com o time de
designers no projeto de novas aplicações e componentes;
- Consegue realizar troubleshooting de comunicação, lógica e
performance característicos de conexão cliente-servidor.

***Diferenciais:***

- React Native
- Python
- Websockets
- Redux

***Como é um típico dia na Sieve?***

Nosso time trabalha com a metodologia Scrum, com sprints de 2 semanas.
Porém, todo o time tem autonomia de desenvolver produtos do seu próprio
jeito, incluindo metodologias e tecnologias. Somos movidos a resultados
e trabalhamos para entregar soluções valiosas para nossos clientes.
Amamos pair-programming e encorajamos as interações diárias porque
acreditamos que o sucesso vem da diversidade, autonomia e colaboração.
Como uma unidade de negócio da B2W, líder de e-commerce na América
Latina, trabalhamos em um ambiente “open space”, com diversas áreas e
espaços colaborativos divididos com times multidisciplinares.

***Como funciona o nosso processo seletivo?***

Nosso processo começa com uma triagem de currículos e experiências.
Em seguida, enviamos um desafio técnico prático para o candidato
resolver no seu próprio prazo. O desafio é proposto para avaliarmos a
capacidade do candidato em resolver um problema, sua linguagem de
programação e habilidades técnicas. Acreditamos que encontrar boas
pessoas é um trabalho tanto dos candidatos quanto dos recrutadores, por
isso, sempre enviamos um Feedback completo com likes e dislikes. Quando
bem sucedido, nosso processo é finalizado com uma visita ao nosso
escritório, onde o candidato conhece nosso time, faz perguntas, conversa
sobre a execução do seu desafio e nos conta sobre a sua experiência.

***Benefícios***

- VR;
- VT;
- Plano de Saúde;
- Plano Odontológico;
- Seguro de vida;
- Oportunidade de trabalhar na maior companhia de e-commerce da América Latina;
- Salário negociável, equivalente à experiência do candidato;
- Horário Flexível;
- Ambiente agradável e acolhedor;
- Trabalho dinâmico e desafiador.
